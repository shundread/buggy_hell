#!/bin/sh

fontFile="exported/berry.fnt"

# Grab font name:
fontFace=$(xmlstarlet sel -t -v "font/info/@face" "${fontFile}")
oldPageFile=$(xmlstarlet sel -t -v "font/pages/page[@id=0]/@file" "${fontFile}")

echo "Font face is ${fontFace}"
echo "Page file is ${oldPageFile}"
