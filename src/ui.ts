// External libraries
import * as PIXI from "pixi.js";

// Game libraries
import * as DisplayInfo from "./displayInfo";

// Main PIXI objects, layers & top-level user interface elements
let app: PIXI.Application;
let renderer: PIXI.SystemRenderer;
let stage: PIXI.Container;
let stageMask: PIXI.Graphics;

function createRenderer() {
  const displayInfo: DisplayInfo.IDisplayInfo = DisplayInfo.getDisplayInfo();

  // TODO set this individually on tiles instead
  // PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;

  // tslint:disable:object-literal-sort-keys
  app = new PIXI.Application({
    width: displayInfo.renderer.width,
    height: displayInfo.renderer.height,
    resolution: 1,
    backgroundColor: 0x000000,
    roundPixels: true,
    sharedLoader: true,
    sharedTicker: true,
  });
  // tslint:enable:object-literal-sort-keys

  // Crate the main stage
  stage = new PIXI.Container();

  // Mask the main stage
  stageMask = new PIXI.Graphics();
  stage.addChild(stageMask);
  stage.mask = stageMask;

  // Attach the main stage to the app
  app.stage.addChild(stage);

  renderer = app.renderer;
  document.body.appendChild(app.view);
  window.addEventListener("resize", handleResize);

  handleResize();
}

function handleResize() {
  const info: DisplayInfo.IDisplayInfo = DisplayInfo.getDisplayInfo();
  console.log("Handling resize, new display info is:", info);

  // Redo the main stage mask
  stageMask.clear();
  stageMask.beginFill(0xffffff);
  stageMask.drawRect(0, 0, DisplayInfo.Width, DisplayInfo.Height);
  stageMask.endFill();
  stageMask.pivot.set(0.5 * DisplayInfo.Width, 0.5 * DisplayInfo.Height);

  // Resize the renderer
  renderer.resize(info.renderer.width, info.renderer.height);

  // Adjust scale and rotation, center main stage
  stage.scale.set(info.scale, info.scale);
  stage.rotation = info.rotation;
  stage.position.set(0.5 * info.renderer.width, 0.5 * info.renderer.height);
}

export {
  createRenderer,
  stage,
}
