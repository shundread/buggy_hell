// External libraries
import * as PIXI from "pixi.js";
import * as TWEEN from "tween.js";

// Generated libraries
import * as Audio from "./gen/audio";

// Game libraries
import * as Image from "./image";
import * as UI from "./ui";
import { MainMenu } from "./mainMenu";
import { MatchScene } from "./matchScene";
import { waitTween } from "./waiter";

const FadeSceneDuration: number = 500;

// Main view
let mainMenu: MainMenu
let matchScene: MatchScene;

function boot() {
  UI.createRenderer();
  PIXI.ticker.shared.add(tick);
  loadGameData();
}

let lastTime: number = Date.now();

function tick() {
  TWEEN.update();

  const now: number = Date.now();
  const deltaMiliseconds: number = now - lastTime;
  if (matchScene !== undefined) {
    matchScene.update(now, deltaMiliseconds);
  }
  lastTime = now;
}

function loadGameData() {
  // Audio
  Audio.load();

  // Images
  Image.loadRemainingSpritesheets();

  // Fonts
  PIXI.loader.add("fontGumy", "./fonts/gummy.xml");

  // Start load and set completion callback
  PIXI.loader.load(onLoadDone);
}

async function onLoadDone() {
  console.log("Resources loaded:", PIXI.loader.resources);

  // Initiialize sounds
  await Audio.initialize();

  // Initialize spritesheets
  Image.initializeRemainingSpritesheets();

  // Start theme
  Audio.Sounds.theme.play({ loop: true });

  // Add match screen
  mainMenu = new MainMenu(() => onStartGame());
  matchScene = new MatchScene(() => onEndGame());
  UI.stage.addChild(mainMenu.stage);

  await waitTween(new TWEEN.Tween(mainMenu.stage)
    .to({ alpha: 1 }, FadeSceneDuration));
  mainMenu.start();
}

async function onStartGame() {
  console.log("on start game");

  // Stop & remove main menu
  mainMenu.stop();
  await waitTween(new TWEEN.Tween(mainMenu.stage)
    .to({ alpha: 0 }, FadeSceneDuration));
  UI.stage.removeChild(mainMenu.stage);
  mainMenu.reset();

  // Add & boot matching scene
  UI.stage.addChild(matchScene.stage);
  await waitTween(new TWEEN.Tween(matchScene.stage)
    .to({ alpha: 1 }, FadeSceneDuration));
  matchScene.start();
}

async function onEndGame() {
  // Stop & remove matching scene
  await matchScene.stop();
  await waitTween(new TWEEN.Tween(matchScene.stage)
    .to({ alpha: 0 }, FadeSceneDuration));
  UI.stage.removeChild(matchScene.stage);
  matchScene.reset();

  // Add & boot main menu
  UI.stage.addChild(mainMenu.stage);
  await waitTween(new TWEEN.Tween(mainMenu.stage)
    .to({ alpha: 1 }, FadeSceneDuration));
  mainMenu.start();
}

// Wait for document to load to start running set-up actions
document.addEventListener("DOMContentLoaded", boot);
