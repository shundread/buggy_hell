// External libraries
import * as PIXI from "pixi.js";
import * as TWEEN from "tween.js";

// Generated libraries
import * as Layout from "../gen/layout";
import * as Spritesheet from "../gen/spritesheet";

// Game libraries
import * as DisplayInfo from "../displayInfo";
// import { waitTween } from "../waiter";

const FadeTextDuration: number = 1000;
const BounceDuration: number = 2000;
const BounceRange: number = 0.05

class MainMenu {
  public stage: PIXI.Container = new PIXI.Container();

  private background: PIXI.Sprite = new PIXI.Sprite(Spritesheet.menuScreen.background);
  private startText: PIXI.Sprite = new PIXI.Sprite(Spritesheet.menuScreen.start);
  private creditsText: PIXI.Sprite = new PIXI.Sprite(Spritesheet.menuScreen.credits);
  private copyrightText: PIXI.Sprite = new PIXI.Sprite(Spritesheet.menuScreen.copyright);

  constructor(private onStartGame: () => void) {
    // Move to top-left
    this.stage.pivot.set(0.5 * DisplayInfo.Width, 0.5 * DisplayInfo.Height);

    this.startText.position.set(
      Layout.menuScreen.start.left,
      Layout.menuScreen.start.top);

    this.creditsText.position.set(
      Layout.menuScreen.credits.left,
      Layout.menuScreen.credits.top);

    this.copyrightText.position.set(
      Layout.menuScreen.copyright.left,
      Layout.menuScreen.copyright.top);

    this.stage.addChild(this.background);
    this.stage.addChild(this.startText);
    this.stage.addChild(this.creditsText);
    this.stage.addChild(this.copyrightText);

    this.stage.on("pointerdown", () => this.onStartGame());

    this.stage.alpha = 0;
    this.startText.alpha = 0;
    this.creditsText.alpha = 0;
    this.copyrightText.alpha = 0;

    this.startText.anchor.y = -BounceRange;
    new TWEEN.Tween(this.startText.anchor)
      .to({ y: BounceRange }, BounceDuration)
      .easing(TWEEN.Easing.Sinusoidal.InOut)
      .yoyo(true)
      .repeat(Infinity)
      .start();
  }

  public start() {
    this.stage.interactive = true;
    new TWEEN.Tween(this.startText)
      .to({ alpha: 1 }, FadeTextDuration)
      .start();
    new TWEEN.Tween(this.creditsText)
      .to({ alpha: 1 }, FadeTextDuration)
      .start();
    new TWEEN.Tween(this.copyrightText)
      .to({ alpha: 1 }, FadeTextDuration)
      .start();
  }

  public stop() {
    this.stage.interactive = false;
  }

  public reset() {
    this.startText.alpha = 0;
    this.creditsText.alpha = 0;
    this.copyrightText.alpha = 0;
  }
}

export {
  MainMenu,
};
