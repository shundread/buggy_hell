// External libraries
import * as TWEEN from "tween.js";

// await-able function for playing a single tween. Does not work with chained
// tweens
async function waitTween(tween: TWEEN.Tween) {
  const promise: Promise<void> = new Promise((resolve: () => void) => {
    tween.onComplete(resolve);
    tween.onStop(resolve);
    tween.start();
  });
  await promise;
}

// await-able function that just waits for a moment
async function wait(miliseconds: number) {
  // Could have been implemented in terms of setting default JS callbacks maybe
  return waitTween(new TWEEN.Tween({}).to({}, miliseconds));
}

export {
  wait,
  waitTween,
};
