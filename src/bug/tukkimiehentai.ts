// External libraries
import * as PIXI from "pixi.js";
// Generated libraries
import * as Spritesheet from "../gen/spritesheet";

// Game libraries
import { Bug, Maturity } from "./bug";

class Tukkimiehentai extends Bug {
  constructor() {
    super(
      "TukkimiehentXi",
      [
        Maturity.Larva,
        Maturity.Cocoon,
        Maturity.Mature,
        Maturity.Dead,
      ],
      [
        Spritesheet.tukkimiehentai.larva as PIXI.Texture,
        Spritesheet.tukkimiehentai.cocoon as PIXI.Texture,
        Spritesheet.tukkimiehentai.mature as PIXI.Texture,
        Spritesheet.tukkimiehentai.dead as PIXI.Texture,
      ],
    );
  }
}

export {
  Tukkimiehentai,
};
