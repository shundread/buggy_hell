// External libraries
import * as PIXI from "pixi.js";
// Generated libraries
import * as Spritesheet from "../gen/spritesheet";

// Game libraries
import { Bug, Maturity } from "./bug";

class Tuohinopsakki extends Bug {
  constructor() {
    super(
      "Tuohinopsakki",
      [
        Maturity.Mature,
        Maturity.Dead,
      ],
      [
        Spritesheet.tuohinopsakki.mature as PIXI.Texture,
        Spritesheet.tuohinopsakki.dead as PIXI.Texture,
      ],
    );
  }
}

export {
  Tuohinopsakki,
};
