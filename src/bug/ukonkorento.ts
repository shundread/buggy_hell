// External libraries
import * as PIXI from "pixi.js";
// Generated libraries
import * as Spritesheet from "../gen/spritesheet";

// Game libraries
import { Bug, Maturity } from "./bug";

class Ukonkorento extends Bug {
  constructor() {
    super(
      "Ukonkorento",
      [
        Maturity.Larva,
        Maturity.Mature,
        Maturity.Dead,
      ],
      [
        Spritesheet.ukonkorento.larva as PIXI.Texture,
        Spritesheet.ukonkorento.mature as PIXI.Texture,
        Spritesheet.ukonkorento.dead as PIXI.Texture,
      ],
    );
  }
}

export {
  Ukonkorento,
};
