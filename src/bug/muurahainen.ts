// External libraries
import * as PIXI from "pixi.js";
// Generated libraries
import * as Spritesheet from "../gen/spritesheet";

// Game libraries
import { Bug, Maturity } from "./bug";

class Muurahainen extends Bug {
  constructor() {
    super(
      "Muurahainen",
      [
        Maturity.Larva,
        Maturity.Mature,
        Maturity.Dead,
      ],
      [
        Spritesheet.muurahainen.larva as PIXI.Texture,
        Spritesheet.muurahainen.mature as PIXI.Texture,
        Spritesheet.muurahainen.dead as PIXI.Texture,
      ],
    );
  }
}

export {
  Muurahainen,
};
