// External libraries
import * as PIXI from "pixi.js";

// Generated libraries
import * as Spritesheet from "../gen/spritesheet";

// Game libraries
import { Bug, Maturity } from "./bug";

class Sinisiipi extends Bug {
  constructor() {
    super(
      "Sinisiipi",
      [
        Maturity.Larva,
        Maturity.Cocoon,
        Maturity.Mature,
        Maturity.Dead,
      ],
      [
        Spritesheet.sinisiipi.larva as PIXI.Texture,
        Spritesheet.sinisiipi.cocoon as PIXI.Texture,
        Spritesheet.sinisiipi.mature as PIXI.Texture,
        Spritesheet.sinisiipi.dead as PIXI.Texture,
      ],
    );
  }
}

export {
  Sinisiipi,
};
