// External libraries
import * as PIXI from "pixi.js";
import * as TWEEN from "tween.js";

// Generated libraries
import * as Spritesheet from "../gen/spritesheet";

// Game libraries
import { IColor, interpolateColors, rgbToTint } from "../util";
import { waitTween } from "../waiter";

enum Maturity {
  Larva  = "Larva",
  Cocoon = "Cocoon",
  Mature = "Mature",
  Dead   = "Dead",
};

// Colors
const White: IColor = { r: 255, g: 255, b: 255 };
const HighlightColorA: IColor = { r: 255, g: 255, b:   0 };
const HighlightColorB: IColor = { r:   0, g: 125, b: 125 };

class Bug {
  public static FlashDuration: number = 1000 / (2 * Math.PI);
  public static AgeDuration: number = 350;
  public static AgeHalf: number = 0.5 * Bug.AgeDuration;
  public static SpawnDuration: number = 0.5 * Bug.AgeDuration;
  public static DeathScaleDuration: number = 500;
  public static DeathFadeDuration: number = 500;
  public static FadeDuration: number = 500;


  public sprite: PIXI.Sprite;
  public scale: number = 1;

  public stage: number = 0;
  private highlighted: boolean = false;

  private particle: PIXI.Sprite | undefined;

  constructor(
    public name: string,
    public stages: Maturity[],
    private textures: PIXI.Texture[],
  ) {
    if (stages.length !== textures.length) {
      throw new Error(`Length of stages is different than length of textures`);
    }

    this.sprite = new PIXI.Sprite(this.textures[this.stage]);
    this.sprite.anchor.set(0.5, 0.5);
  }

  public setScale(scale: number) {
    this.scale = scale;
    this.sprite.scale.set(0, 0);
    new TWEEN.Tween(this.sprite.scale)
      .to({ x: this.scale, y: this.scale }, Bug.SpawnDuration)
      .start();
  }

  public update(now: number) {
    if (this.highlighted) {
      const k: number = 0.5 * (1 + Math.sin(now / Bug.FlashDuration));
      this.sprite.tint = rgbToTint(interpolateColors(k, HighlightColorA, HighlightColorB));
    }
  }

  public async doAge() {
    // Advance age
    this.stage++;

    // Case 0: It's still alive
    if (this.isAlive()) {
      await waitTween(new TWEEN.Tween(this.sprite.scale)
        .to({ x: 0.25 * this.scale, y: 0.25 * this.scale }, Bug.AgeHalf));
      this.sprite.texture = this.textures[this.stage];
      await waitTween(new TWEEN.Tween(this.sprite.scale)
        .to({ x: this.scale, y: this.scale }, Bug.AgeHalf));
      return;
    }

    // Case 1: It's dead, but not yet decayed
    if (this.isDecayed() === false) {
      this.sprite.texture = this.textures[this.stage];
      this.particle = new PIXI.Sprite(Spritesheet.particles.death);
      this.particle.anchor.set(0.5, 0.5);
      this.particle.scale.set(0, 0);
      this.sprite.addChild(this.particle);

      // Expand skull
      await waitTween(new TWEEN.Tween(this.particle.scale)
        .to({ x: 0.75, y: 0.75 }, Bug.DeathScaleDuration)
        .easing(TWEEN.Easing.Bounce.Out));

      // Fade skull
      await waitTween(new TWEEN.Tween(this.particle)
        .to({ alpha: 0 }, Bug.DeathFadeDuration)
        .easing(TWEEN.Easing.Quartic.In));

      // Remove particle effect
      this.sprite.removeChild(this.particle);
      this.particle.destroy();
      this.particle = undefined;
      return;
    }

    // Case 2: It's decaying
    new TWEEN.Tween(this.sprite)
      .to({ alpha: 0 }, Bug.FadeDuration)
      .start();
  }

  public isAlive(): boolean {
    return (this.isDecayed() === false) && (this.stages[this.stage] !== Maturity.Dead);
  }

  public isDecayed(): boolean {
    return this.stage >= this.stages.length;
  }

  public setHighlighted(value: boolean) {
    this.highlighted = value;
    if (value === false) {
      this.sprite.tint = rgbToTint(White);
    }
  }

  public destroy() {
    this.setHighlighted(false);
    if (this.particle) {
      this.sprite.removeChild(this.particle);
      this.particle.destroy();
    }
    this.sprite.destroy();
  }
}

export {
  Bug,
  Maturity,
}
