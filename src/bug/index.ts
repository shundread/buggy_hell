import { Bug } from "./bug";
import { Muurahainen } from "./muurahainen";
import { Sinisiipi } from "./sinisiipi";
import { Tukkimiehentai } from "./tukkimiehentai";
import { Tuohinopsakki } from "./tuohinopsakki";
import { Ukonkorento } from "./ukonkorento";

export {
  Bug,
  Muurahainen,
  Sinisiipi,
  Tukkimiehentai,
  Tuohinopsakki,
  Ukonkorento,
};
