// External libraries
import * as PIXI from "pixi.js";
import * as TWEEN from "tween.js";

// Generated libraries
import * as Audio from "../gen/audio";
import * as Font from "../gen/font";
import * as Layout from "../gen/layout";
import * as Spritesheet from "../gen/spritesheet";

// Game libraries
import {
  Bug,
  Muurahainen,
  Sinisiipi,
  Tukkimiehentai,
  Tuohinopsakki,
  Ukonkorento,
} from "../bug";
import * as DisplayInfo from "../displayInfo";
import { wait, waitTween } from "../waiter";

// Score levels
const MatchPoor: number = 1;
const MatchOk: number = 4;
const MatchGood: number = 8;
const MatchGreat: number = 12;

// Grid layout
const Rows: number = 6;
const Cols: number = 5;

const StartingDifficulty: number = 2;
const ToLevel3: number = 50;
const ToLevel4: number = 125;
const ToLevel5: number = 250;

// Bug layout information
const BugWidth: number = Layout.matchScreen.matchArea.width / Cols;
const BugHeight: number = Layout.matchScreen.matchArea.height / Rows;
const FramePaddingX: number = Layout.matchScreen.matchArea.left - Layout.matchScreen.matchAreaFrame.left;
const FramePaddingY: number = Layout.matchScreen.matchArea.top - Layout.matchScreen.matchAreaFrame.top;
const BugPaddingX: number = (0.5 * BugWidth) + FramePaddingX;
const BugPaddingY: number = (0.5 * BugHeight) + FramePaddingY;

// Timer information
const TimerMax: number = 15000;
const TimerLow: number = 4000;
const ScoreTimerFactor: number = 200;

// Durations
const FadeBoardDuration: number = 1000;
const PointerFadeDuration: number = 1000;
const ScoreSlideDuration: number = 1000;
const ScoreLingerDuration: number = 1750;

const TextStyle: PIXI.extras.BitmapTextStyle = {
  align: "center",
  font: Font.gummy,
  tint: 0x000000,
};

const PointerTextStyle: PIXI.extras.BitmapTextStyle = {
  align: "center",
  font: Font.gummy,
  tint: 0xf57900,
};

type GridCell = Bug | undefined;
type PointerCell = PIXI.extras.BitmapText | undefined;

class MatchScene {
  public stage: PIXI.Container = new PIXI.Container();

  // Main layout elements
  private background: PIXI.Sprite = new PIXI.Sprite(Spritesheet.matchBackground.background);
  private scoreTitle: PIXI.extras.BitmapText = new PIXI.extras.BitmapText("Score", TextStyle);
  private scoreIndicator: PIXI.extras.BitmapText = new PIXI.extras.BitmapText("0", TextStyle);
  private matchArea: PIXI.Sprite = new PIXI.Sprite(Spritesheet.matchScreen.matchArea);
  private pointerArea: PIXI.Container = new PIXI.Container();
  private timerFrame: PIXI.Sprite = new PIXI.Sprite(Spritesheet.matchScreen.timerFrame);

  // Timer
  private timerLeft: PIXI.Sprite = new PIXI.Sprite(Spritesheet.matchScreen.timer);
  private timerUsed: PIXI.Graphics = new PIXI.Graphics();
  private timerMask: PIXI.Graphics = new PIXI.Graphics();
  private timer: number = TimerMax;

  // Bug grid & difficulty
  private bugs: GridCell[][] = [];
  private difficulty: number = StartingDifficulty;
  private score: number = 0;

  // Matching route
  private matcher: Bug | undefined;
  private matchPath: PIXI.Point[] = [];

  // Score particles
  private pointers: PointerCell[] = [];
  private totalPointer: PIXI.extras.BitmapText = new PIXI.extras.BitmapText("", PointerTextStyle);
  private playing: boolean = false;

  constructor(private onEndGame: () => void) {
    // Move to top-left
    this.stage.pivot.set(0.5 * DisplayInfo.Width, 0.5 * DisplayInfo.Height);

    this.scoreTitle.position.set(
      Layout.matchScreen.scoreTitle.centerX,
      Layout.matchScreen.scoreTitle.centerY);
    this.scoreTitle.anchor = new PIXI.Point(0.5, 0.5);

    this.scoreIndicator.position.set(
      Layout.matchScreen.scoreIndicator.centerX,
      Layout.matchScreen.scoreIndicator.centerY);
      this.scoreIndicator.anchor = new PIXI.Point(0.5, 0.5);

    this.totalPointer.position.set(
      Layout.matchScreen.scoreAdded.centerX,
      Layout.matchScreen.scoreAdded.centerY);
    this.totalPointer.anchor = new PIXI.Point(0.5, 0.5);

    this.matchArea.position.set(
      Layout.matchScreen.matchAreaFrame.left,
      Layout.matchScreen.matchAreaFrame.top);

    this.pointerArea.position.set(
      this.matchArea.position.x,
      this.matchArea.position.y);

    this.timerFrame.position.set(
      Layout.matchScreen.timerFrame.left,
      Layout.matchScreen.timerFrame.top);

    this.timerLeft.position.set(
      Layout.matchScreen.timer.left,
      Layout.matchScreen.timer.top);

    this.timerUsed.beginFill(0x003300);
    this.timerUsed.drawRect(
      Layout.matchScreen.timer.left,
      Layout.matchScreen.timer.top,
      Layout.matchScreen.timer.width,
      Layout.matchScreen.timer.height);
    this.timerUsed.endFill();

    // Mask the timer left
    this.timerMask.isMask = true;
    this.timerLeft.mask = this.timerMask;
    this.timerLeft.addChild(this.timerMask);
    this.updateTimer();

    // Setup layout
    this.stage.addChild(this.background);
    this.stage.addChild(this.scoreTitle);
    this.stage.addChild(this.scoreIndicator);
    this.stage.addChild(this.totalPointer);
    this.stage.addChild(this.matchArea);
    this.stage.addChild(this.pointerArea);
    this.stage.addChild(this.timerUsed);
    this.stage.addChild(this.timerLeft);
    this.stage.addChild(this.timerFrame);

    // Make the grid
    for (let y = 0; y < Rows; y++) {
      const row = [];
      for (let x = 0; x < Cols; x++) {
        row.push(undefined);
      }
      this.bugs.push(row);
    }

    // Make the matching area interactive
    this.stage.on("pointerdown", (event: PIXI.interaction.InteractionEvent) => this.matchBegin(event));
    this.stage.on("pointermove", (event: PIXI.interaction.InteractionEvent) => this.matchMove(event));
    this.stage.on("pointerup", (event: PIXI.interaction.InteractionEvent) => this.matchEnd(event));
    this.stage.on("pointerout", (event: PIXI.interaction.InteractionEvent) => this.matchEnd(event));
    this.stage.on("pointerupoutside", (event: PIXI.interaction.InteractionEvent) => this.matchEnd(event));

    this.stage.alpha = 0;
  }

  ///////////////////////////
  // Start / Stop the game //
  ///////////////////////////

  public start() {
    this.difficulty = StartingDifficulty;
    this.stage.interactive = true;
    this.playing = true;
    this.spawnBugs();
  }

  public async stop() {
    this.stage.interactive = false;
    this.playing = false;
    await waitTween(new TWEEN.Tween(this.matchArea)
      .to({ alpha: 0 }, FadeBoardDuration));
    new TWEEN.Tween(this.scoreTitle.position)
      .to({
        x: Layout.matchScreen.scoreTitleCenter.centerX,
        y: Layout.matchScreen.scoreTitleCenter.centerY,
      }, ScoreSlideDuration)
      .easing(TWEEN.Easing.Quartic.InOut)
      .start();
    new TWEEN.Tween(this.scoreIndicator.position)
      .to({
        x: Layout.matchScreen.scoreIndicatorCenter.centerX,
        y: Layout.matchScreen.scoreIndicatorCenter.centerY,
      }, ScoreSlideDuration)
      .easing(TWEEN.Easing.Quartic.InOut)
      .start();
    await wait(ScoreSlideDuration + ScoreLingerDuration);
  }

  public reset() {
    // Reset timer / score / difficulty
    this.timer = TimerMax;
    this.score = 0;
    this.difficulty = StartingDifficulty;
    this.scoreIndicator.text = "0";
    this.totalPointer.text = "";

    // Fix resource alphas and positions
    this.matchArea.alpha = 1;

    this.scoreTitle.position.set(
      Layout.matchScreen.scoreTitle.centerX,
      Layout.matchScreen.scoreTitle.centerY);
    this.scoreIndicator.position.set(
      Layout.matchScreen.scoreIndicator.centerX,
      Layout.matchScreen.scoreIndicator.centerY);


    // Empty the board
    for (let y = 0; y < Rows; y++) {
      for (let x = 0; x < Cols; x++) {
        const bug: Bug | undefined = this.bugs[y][x];
        if (bug) {
          this.removeBug(x, y);
        }
      }
    }
  }

  //////////////////
  // Update magic //
  //////////////////
  public async update(now: number, deltaMiliseconds: number) {
    if (this.playing) {
      this.timer = Math.max(0, this.timer - deltaMiliseconds);
      this.updateTimer();
    }

    // Update bugs
    for (let y = 0; y < Rows; y++) {
      for (let x = 0; x < Cols; x++) {
        const bug: Bug | undefined = this.bugs[y][x];
        if (bug) {
          bug.update(now);
        }
      }
    }

    // Clear finished pointers
    for (const [index, pointer] of this.pointers.entries()) {
      if (pointer !== undefined && pointer.alpha === 0) {
        this.pointerArea.removeChild(pointer);
        pointer.destroy();
        this.pointers[index] = undefined;
      }
    }
    this.pointers = this.pointers.filter((pointer: PointerCell) => pointer !== undefined);
  }

  //////////////////
  // Timer update //
  //////////////////
  private updateTimer() {
    const width: number = (this.timer / TimerMax) * Layout.matchScreen.timer.width;

    this.timerMask.clear();
    this.timerMask.beginFill(0xffffff);
    this.timerMask.drawRect(0, 0, width, Layout.matchScreen.timer.height);
    this.timerMask.endFill();

    if (this.timer === 0 && this.playing) {
      Audio.Sounds.timerShort.stop();
      Audio.Sounds.timerOver.play();
      this.onEndGame();
    } else if (this.timer < TimerLow) {
      if (Audio.Sounds.timerShort.isPlaying === false) {
        Audio.Sounds.timerShort.play({ loop: true });
      }
    } else {
      if (Audio.Sounds.timerShort.isPlaying) {
        Audio.Sounds.timerShort.stop();
      }
    }
  }

  //////////////////
  // Bug spawning //
  //////////////////

  private spawnBugs() {
    for (let y = 0; y < Rows; y++) {
      for (let x = 0; x < Cols; x++) {
        if (this.bugs[y][x] === undefined) {
          // Spawn the bug
          const bug = this.spawnBug();

          // Scale the bug
          const scale: number = BugWidth / bug.sprite.texture.width;
          bug.setScale(scale);

          bug.sprite.position.x = BugPaddingX + (x * BugWidth);
          bug.sprite.position.y = BugPaddingY + (y * BugHeight);

          // Add the bug to the scene;
          this.bugs[y][x] = bug;
          this.matchArea.addChild(bug.sprite);
        }
      }
    }
  }

  private spawnBug(): Bug {
    const roll: number = Math.floor(Math.random() * this.difficulty);
    switch(roll) {
      case 0: return new Sinisiipi();
      case 1: return new Tukkimiehentai();
      case 2: return new Muurahainen();
      case 3: return new Ukonkorento();
      case 4: return new Tuohinopsakki();
      default: throw new Error(`No spawner for bug at roll result ${roll}`);
    }
  }

  private removeBug(x: number, y: number) {
    const bug: Bug | undefined = this.bugs[y][x];
    if (bug === undefined) {
      console.warn(`Attempting to remove a non-existing bug from cell (${x}, ${y})`);
      return;
    }

    this.matchArea.removeChild(bug.sprite);
    bug.destroy();
    this.bugs[y][x] = undefined;
  }

  /////////////////
  // Age cycling //
  /////////////////

  private async ageCycle() {
    this.stage.interactive = false;
    for (let y = 0; y < Rows; y++) {
      for (let x = 0; x < Cols; x++) {
        const bug: Bug | undefined = this.bugs[y][x];
        if (bug && bug.isDecayed() === false) {
          bug.doAge();
        }
      }
    }

    await wait(Bug.FadeDuration);
    this.stage.interactive = true;

    for (let y = 0; y < Rows; y++) {
      for (let x = 0; x < Cols; x++) {
        const bug: Bug | undefined = this.bugs[y][x];
        if (bug && bug.isDecayed()) {
          this.removeBug(x, y);
        }
      }
    }

    this.spawnBugs();
  }

  ////////////////////
  // Input handlers //
  ////////////////////

  private matchBegin(event: PIXI.interaction.InteractionEvent) {
    const cell: PIXI.Point = this.touchToGrid(event);
    if (!validCell(cell)) { return; }

    const bug = this.bugs[cell.y][cell.x]
    // Special case, we started from an empty cell
    if (bug === undefined) {
      return;
    }
    bug.setHighlighted(true);
    this.matcher = bug;
    this.matchPath.push(cell);
  }

  private matchMove(event: PIXI.interaction.InteractionEvent) {
    const cell: PIXI.Point = this.touchToGrid(event);

    // Special case 0: We don't have a matcher
    if (this.matcher === undefined) {
      return;
    }

    // Special case 1: We're dragging without a button
    if (!event.data.buttons) {
      this.matchEnd(event);
      return;
    }

    // Special case 2: We have left the drag area
    if (!validCell(cell)) {
      this.matchEnd(event);
      return;
    }

    // Special case 3: We are backtracking
    for (const [index, pathCell] of this.matchPath.entries()) {
      if (pathCell.x === cell.x && pathCell.y === cell.y) {
        // Un-highlight all the tailing bugs
        const trimmed: PIXI.Point[] = this.matchPath.slice(index + 1);
        for (const trimmedCell of trimmed) {
          const bug: Bug = this.bugs[trimmedCell.y][trimmedCell.x] as Bug;
          bug.setHighlighted(false);
        }

        // Update the path to contain up to the point we backtracked
        this.matchPath = this.matchPath.slice(0, index + 1);
        return;
      }
    }

    // Special case 4: There is no bug
    const bug: Bug | undefined = this.bugs[cell.y][cell.x];
    if (bug === undefined) {
      return;
    }

    // Special case 5: We are pointing at a non-matching bug
    if (bug.name !== this.matcher.name) {
      return;
    }

    // Special case 6: We are pointing at a non-adjacent cell
    const latestPath: PIXI.Point = this.matchPath[this.matchPath.length - 1];
    if (Math.abs(cell.x - latestPath.x) + Math.abs(cell.y - latestPath.y) !== 1) {
      return;
    }

    bug.setHighlighted(true);
    this.matchPath.push(cell);
  }

  private matchEnd(event: PIXI.interaction.InteractionEvent) {
    const grid: PIXI.Point = this.touchToGrid(event);

    // Special case 0: We don't have a matcher
    if (this.matcher === undefined) {
      return;
    }

    console.log(`End match(${grid.x}, ${grid.y})`);
    console.log(`Matched the following:`);
    let bugPoints: number = 0;
    let lengthPoints: number = 0;
    for (const cell of this.matchPath) {
      console.log(`  ${this.matcher.name} at (${cell.x}, ${cell.y})`);
      const bug = this.bugs[cell.y][cell.x];
      if (bug === undefined) {
        continue;
      }
      let bugValue: number = 0;
      if (bug.isAlive()) {
        lengthPoints++;
        bugValue = (bug.stage + 1) * lengthPoints;
      }
      bugPoints += bugValue;

      const pointer = new PIXI.extras.BitmapText(`${bugValue}`, PointerTextStyle);
      pointer.anchor = new PIXI.Point(0.5, 0.5);
      pointer.position.set(bug.sprite.position.x, bug.sprite.position.y);
      this.pointerArea.addChild(pointer);
      new TWEEN.Tween(pointer)
        .to({ alpha: 0 }, PointerFadeDuration)
        .easing(TWEEN.Easing.Quintic.In)
        .start();
      this.removeBug(cell.x, cell.y);
    }

    // Calculate score from bug points and length points, play match sound, indicate total
    const score = bugPoints;
    playMatchSound(score);

    this.totalPointer.text = `${this.matcher.name} - ${score}`;
    this.totalPointer.alpha = 1;
    new TWEEN.Tween(this.totalPointer)
      .to({ alpha: 0 }, PointerFadeDuration)
      .easing(TWEEN.Easing.Quintic.In)
      .start();

    // Calculate and update score text
    this.score += score;
    this.scoreIndicator.text = `${this.score}`;

    // Test for difficulty increase
    if (this.score > ToLevel5 && this.difficulty < 5) {
      this.difficulty = 5;
    }
    if (this.score > ToLevel4 && this.difficulty < 4) {
      this.difficulty = 4;
    }
    if (this.score > ToLevel3 && this.difficulty < 3) {
      this.difficulty = 3;
    }

    // Bump up timer
    const timerBonus: number = bugPoints * ScoreTimerFactor;
    this.timer = Math.min(TimerMax, this.timer + timerBonus);

    // TODO show timer & score on-screen

    this.matcher = undefined;
    this.matchPath = [];
    this.ageCycle();
    this.spawnBugs();
  }

  private touchToGrid(event: PIXI.interaction.InteractionEvent): PIXI.Point {
    const local: PIXI.Point = this.matchArea.toLocal(event.data.global);
    return new PIXI.Point(
      Math.floor(local.x / BugWidth),
      Math.floor(local.y / BugHeight));
  }
}

function validCell(point: PIXI.Point): boolean {
  return point.x >= 0 && point.y >= 0 && point.x < Cols && point.y < Rows;
}

function playMatchSound(score: number) {
  if (score < MatchPoor) {
    Audio.Sounds.match0.play();
    return;
  }
  if (score < MatchOk) {
    Audio.Sounds.match1.play();
    return;
  }
  if (score < MatchGood) {
    Audio.Sounds.match2.play();
    return;
  }
  if (score < MatchGreat) {
    Audio.Sounds.match3.play();
    return;
  }
  Audio.Sounds.match4.play();
  return;
}

export {
  MatchScene,
}
