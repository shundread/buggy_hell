interface ILayoutInfo {
  width: number;
  height: number;

  left: number;
  top: number;

  centerX: number;
  centerY: number;

  right: number;
  bottom: number;
}

export {
  ILayoutInfo,
};
