interface IColor {
  r: number;
  g: number;
  b: number;
}

function rgbToTint(color: IColor): number {
  const rComponent = Math.floor(color.r) * 256 * 256;
  const gComponent = Math.floor(color.g) * 256;
  const bComponent = Math.floor(color.b);
  return rComponent + gComponent + bComponent;
}

function interpolateColors(k: number, start: IColor, end: IColor): IColor {
  return {
    b: (1 - k) * start.b + k * end.b,
    g: (1 - k) * start.g + k * end.g,
    r: (1 - k) * start.r + k * end.r,
  };
}

export {
  IColor,
  interpolateColors,
  rgbToTint,
};
