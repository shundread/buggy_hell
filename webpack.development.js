const config = require('./webpack.common');
config.mode = "development";
config.devtool = "source-map";
config.optimization = {
  minimize: false,
};
module.exports = config;
