#!/bin/sh

. "$(dirname $0)/common.sh"

# Clear whole work folder
rm -rf --verbose "work"
rm --verbose -rf "build"

# Resolve build type:
# accept only 'production' as argument, or default to 'development' build
if [ "${#}" = "1" ]; then
  if [ "$1" = "production" ]; then
    build="production"
  else
    echo "The only accepted arguments for the make script are none or 'production'"
    exit 1
  fi
else
  echo "Making a development build. Run '$0 production' if you wish to make a production build"
  build="development"
fi

# Announce build type
if [ "${build}" = "production" ]; then
  buildEcho="${lightGreen}(PRODUCTION BUILD)${noColor}"
else
  buildEcho="${yellow}(DEVELOPMENT BUILD)${noColor}"
fi
echo
echo
echo "Building ${lightPurple}Buggy Hello ${buildEcho}"
echo
echo

# Ship static files
cp --verbose -rf "static" "build"

# Set path for node binaries
PATH=${PATH}:./node_modules/.bin

# Build code generation steps first to avoid compiling whole project with
# outdated generated files
./make/audio.sh
./make/fonts.sh
./make/graphics.sh

# Compilation TODO split to make-compile.sh
echo "Compiling..."
if [ "${build}" = "production" ]; then
  ./make/compile.sh "production"
else
  ./make/compile.sh
fi
