#!/bin/sh

. "$(dirname $0)/common.sh"

# Important paths
buildFolder="build/fonts"
sourceFolder="fonts/exported"
workFolder="work/fonts"
fontNamesFile="./src/gen/font.ts"

echo
echo
echo "${yellow}Bundling fonts${noColor}"
echo
echo

# Clear fonts build folder
if [ ! -d "build" ]; then
  mkdir --verbose "build"
fi
rm -rf --verbose "${buildFolder}"
mkdir --verbose "${buildFolder}"

# Clear font work folder
if [ ! -d "work" ]; then
  mkdir --verbose "work"
fi
rm -rf --verbose "${workFolder}"
mkdir --verbose "${workFolder}"

# Fonts packaging
echo "Updating fonts"

updateFile()
{
  # Trim relevant part of file name for the routine
  trimmed=$(echo "${1}" | sed "s/.fnt//" | sed "s!./!!")

  inFile="${sourceFolder}/${trimmed}.fnt"
  outFile="${buildFolder}/${trimmed}.xml"

  # Fix page file path & copy page file
  xmlstarlet edit \
    --update 'font/pages/page[@id=0]/@file' \
    --value "${trimmed}.png" \
    "${inFile}" > "${outFile}"
  echo "${inFile}" "==(t)==>" "${outFile}"
  cp --verbose "${sourceFolder}/${trimmed}_0.png" "${buildFolder}/${trimmed}.png"

  # Render font information onto a typescript snippet
  fontFace=$(xmlstarlet sel --template --value-of "font/info/@face" "${outFile}")
  fontSize=$(xmlstarlet sel --template --value-of "font/info/@size" "${outFile}")
  fontName="${fontSize}px ${fontFace}"
  echo "export const ${trimmed}: string = \"${fontName}\";" > "${workFolder}/${trimmed}.ts"
}

fontFiles=$(cd "${sourceFolder}" && ls ./*.fnt)
for fontFile in ${fontFiles}
do
  updateFile ${fontFile}
done

rm -f --verbose "${fontNamesFile}"
{
  echo "// Auto-generated file, do not edit directly but run make-fonts.sh instead"
  echo ""
  cat "${workFolder}"/*.ts
} >> "${fontNamesFile}"
echo "Generated ${fontNamesFile}"
