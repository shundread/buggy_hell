#!/bin/sh

. "$(dirname $0)/common.sh"

# Set path for node binaries
PATH=${PATH}:./node_modules/.bin

# Important paths
buildFolder="build/graphics"
sourceFolder="graphics"
workFolder="work/graphics"
spritesheetFile="./src/gen/spritesheet.ts"
layoutFile="./src/gen/layout.ts"

echo
echo
echo "${yellow}Bundling graphics${noColor}"
echo
echo

# Clear graphics build folder
if [ ! -d "build" ]; then
  mkdir --verbose "build"
fi
rm -rf --verbose "${buildFolder}"
mkdir --verbose "${buildFolder}"

# Clear font work folder
if [ ! -d "work" ]; then
  mkdir --verbose "work"
fi
rm -rf --verbose "${workFolder}"
mkdir --verbose "${workFolder}"

# Graphics packaging
bundleSpritesheet()
{
  folder="${1}"
  printf "Bundling folder %s..." "${workFolder}/${folder}"
  spritesheet-js \
    --format=pixi.js \
    --name="${folder}" \
    --prefix="${folder}_" \
    --path="${buildFolder}" \
    --padding=2 \
    ./"${workFolder}"/"${folder}"/*.png
}

# Spritesheet constant type annotation and declaration
handleSpritesheetFiles()
{
  spritesheet=${1}
  printf "Handling frame files for spritesheet ${spritesheet}..."
  mkdir --verbose "${workFolder}/${folder}"

  files=$(cd "${sourceFolder}"/"${spritesheet}" && ls ./*.png)

  # Field types
  for file in ${files}
  do
    trimmed=$(echo "${file}" | sed "s/.png//" | sed "s!./!!")
    cp --verbose \
      "${sourceFolder}/${folder}/${trimmed}.png" \
      "${workFolder}/${folder}"
  done
}

grabLayoutInfo()
{
  spritesheet=${1}
  files=$(cd "${sourceFolder}"/"${spritesheet}" && ls ./*.xcf)
  for file in ${files}
  do
    trimmed=$(echo "${file}" | sed "s/.xcf//" | sed "s!./!!")
    outputFile="./${workFolder}/${trimmed}.ts_layout"
    ./make/lib/layout.py \
      --verbose \
      --output="${outputFile}" \
      "${sourceFolder}"/"${spritesheet}"/"${trimmed}".xcf
  done
}

folders=$(ls "${sourceFolder}")
for folder in ${folders}
do
  handleSpritesheetFiles "${folder}"
  bundleSpritesheet "${folder}"
  grabLayoutInfo "${folder}"
done

# Generate code for spritesheets
rm -f --verbose "${spritesheetFile}"
./make/lib/spritesheet.py \
  --verbose \
  --recursive \
  --output="${spritesheetFile}" \
  "${sourceFolder}"

# Generate code for layouts
# TODO add recursive execution support on the python script
rm --verbose -f "${layoutFile}"
{
  echo "// Auto-generated file, do not edit directly but run make-graphics.sh instead"
  echo ""
  echo "// Game libraries"
  echo 'import { ILayoutInfo } from "layoutInfo";'
  cat "${workFolder}"/*.ts_layout
} > "${layoutFile}"
echo "Generated ${layoutFile}"
