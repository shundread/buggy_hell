#!/bin/sh

. "$(dirname $0)/common.sh"

# Clear latest build / map
rm -f --verbose "build/bundle.js"
rm -f --verbose "build/bundle.js.map"

# Resolve build type:
# accept only 'production' as argument, or default to 'development' build
if [ "${#}" = "1" ]; then
  if [ "$1" = "production" ]; then
    build="production"
  else
    echo "The only accepted arguments for the make script are none or 'production'"
    exit 1
  fi
else
  echo "Making a development build. Run '$0 production' if you wish to make a production build"
  build="development"
fi

# Announce build type
if [ "${build}" = "production" ]; then
  buildEcho="${lightGreen}(PRODUCTION BUILD)${noColor}"
else
  buildEcho="${yellow}(DEVELOPMENT BUILD)${noColor}"
fi
echo
echo
echo "Compiling ${lightPurple}Buggy Hello ${buildEcho}"
echo
echo

# Set path for node binaries
PATH=${PATH}:./node_modules/.bin

if [ "${build}" = "production" ]; then
  webpack --progress -p --config webpack.production.js
else
  webpack --progress -p --config webpack.development.js
fi

# Announce build completion
if [ "${build}" = "production" ]; then
  buildEcho="${lightGreen}(PRODUCTION BUILD)${noColor}"
else
  buildEcho="${yellow}(DEVELOPMENT BUILD)${noColor}"
fi
echo
echo
echo "Compiled ${lightPurple}Buggy Hello ${buildEcho}"
echo
echo
