#!/bin/sh

. "$(dirname $0)/common.sh"

# Important paths
buildFolder="build/audio"
sourceFolder="audio/"
resourcePath="audio/"
generatedFile="./src/gen/audio.ts"

echo
echo
echo "${yellow}Bundling audio${noColor}"
echo
echo

# Clear audio build folder
if [ ! -d "build" ]; then
  mkdir --verbose "build"
fi
rm -rf --verbose "${buildFolder}"
mkdir --verbose "${buildFolder}"

./make/lib/audio.py \
  --verbose \
  --output="${generatedFile}" \
  --resource-path="${resourcePath}" \
  "${sourceFolder}"

cp --verbose --recursive "${sourceFolder}" "build"
