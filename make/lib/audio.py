#!/usr/bin/env python3

import getopt
import os
import subprocess
import sys

from folder import Folder
import boilerplate

######################
# Typescript support #
######################

TypescriptOutput = '''\
// Auto-generated file, do not edit directly. Run the {script} code generator
// to update the information instead.

// External libraries
import * as PIXI from "pixi.js";

// XXX: We must assign import result to an identifier or the compiler complains
import sound = require("pixi-sound");

// XXX: We must do something with the identifier or the compiler complains
if (!sound) {{
  throw new Error(`Unable to load pixi-sound`);
}}

//////////////////////
// Types and tables //
//////////////////////

{template}

{paths}

let Sounds: ISoundTable<PIXI.sound.Sound>;

/////////////////////////////////////////////////
// Resource loading and initialization support //
/////////////////////////////////////////////////

{functions}

export {{
    ISoundTable,
    SoundPaths,
    Sounds,
    load,
    initialize,
}};
'''

TypescriptFunctions = '''\
function load() {{
  for (const entry in SoundPaths) {{
    PIXI.loader.add(SoundPaths[entry]);
  }}
}}

async function initializeSound(url: string): Promise<PIXI.sound.Sound> {{
  const promise: Promise<PIXI.sound.Sound> =
        new Promise((resolve: (sound: PIXI.sound.Sound) => void) => {{
    PIXI.sound.Sound.from({{
      url: url,
      preload: true,
      loaded: (
            error: Error,
            sound?: PIXI.sound.Sound,
            instance?: PIXI.sound.IMediaInstance,
      ) => {{
        if (!sound && !instance)  {{
          throw new Error(`Cannot load sound "${{url}}": ${{error.message}}`);
        }}
        if (!sound) {{
          throw new Error(`Expected a sound, not an instance for "${{url}}"`);
        }}
        console.log(`Initialized audio "${{url}}" successfully`);
        resolve(sound);
      }},
    }});
  }});
  return await promise;
}}

async function initialize() {{
  Sounds = {{
{initializations}
  }};
}}'''

TypescriptTemplate = '''\
interface ISoundTable<T> {{
{entries}

  [name: string]: T;
}}'''

TypescriptPaths = '''\
const SoundPaths: ISoundTable<string> = {{
{entries}
}};'''

TypescriptInitializationEntry = '    {name}: await initializeSound("{path}"),'
TypescriptTemplateEntry = '  {name}: T;'
TypescriptPathEntry = '  {name}: "{path}",'


class AudioInfo(object):
    def __init__(self, folder, path):
        self.path = path
        self.prefix = '_'.join(os.path.split(folder.path))
        self.sounds = [
            sound.split('.wav')[0]
            for sound in folder.get_files('.wav')
        ]
        self.sounds.sort()
        self.songs = [
            song.split('.ogg')[0]
            for song in folder.get_files('.ogg')
        ]
        self.songs.sort()
        for sound in self.sounds:
            if sound in self.songs:
                raise ValueError(
                    'Name conflict: Song and Sound with same name "{name}"'
                    .format(name=sound))

    ######################
    # Typescript support #
    ######################
    @property
    def typescript(self):
        return TypescriptOutput.format(script=os.path.basename(sys.argv[0]),
                                       template=self.typescript_template,
                                       paths=self.typescript_paths,
                                       functions=self.typescript_functions)

    @property
    def typescript_functions(self):
        soundEntries = '\n'.join([
            TypescriptInitializationEntry.format(
                name=sound, path=self.typescript_sound_path(sound))
            for sound in self.sounds
        ])
        songEntries = '\n'.join([
            TypescriptInitializationEntry.format(
                name=song, path=self.typescript_song_path(song))
            for song in self.songs
        ])
        entries = '\n'.join([soundEntries, songEntries])
        return TypescriptFunctions.format(initializations=entries)

    @property
    def typescript_template(self):
        resources = self.sounds + self.songs
        entries = '\n'.join([
            TypescriptTemplateEntry.format(name=resource)
            for resource in resources
        ])
        return TypescriptTemplate.format(entries=entries)

    @property
    def typescript_paths(self):
        soundEntries = '\n'.join([
            TypescriptPathEntry.format(
                name=sound, path=self.typescript_sound_path(sound))
            for sound in self.sounds
        ])
        songEntries = '\n'.join([
            TypescriptPathEntry.format(
                name=song, path=self.typescript_song_path(song))
            for song in self.songs
        ])
        entries = '\n'.join([soundEntries, songEntries])
        return TypescriptPaths.format(entries=entries)

    def typescript_sound_path(self, resource):
        return self.path + '/' + resource + '.wav'

    def typescript_song_path(self, resource):
        return self.path + '/' + resource + '.{ogg,mp3}'


##################################
# Command-line execution support #
##################################

Description = '''\
Writes declarations and definitions of audio resources for all audio files
within a given folder.'''


def main():
    boilerplate.parser.usage = '%prog [options] FOLDER'
    boilerplate.parser.description = Description
    boilerplate.add_output_option()
    boilerplate.add_resource_path_option()
    boilerplate.add_verbose_option()

    # Grab command-line options
    (options, arguments) = boilerplate.parse_options(1)
    outstream = options.output

    # Access and write
    walker = os.walk(arguments[0].rstrip('/'))
    folder = Folder(*next(walker))
    boilerplate.debug('  Parsing folder "{}"\n'.format(folder.path))

    audio_info = AudioInfo(folder, options.resource_path.rstrip('/'))
    outstream.write(audio_info.typescript)

    boilerplate.debug('  Audio definitions written to "{}".\n'
                      .format(outstream.name))

if __name__ == '__main__':
    main()
