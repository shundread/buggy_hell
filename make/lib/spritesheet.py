#!/usr/bin/env python3

import getopt
import os
import sys

from folder import Folder
import boilerplate

######################
# Typescript support #
######################

TypescriptHeader = '''\
// Auto-generated file, do not edit directly. Run spritesheet.py to update the
// information instead.

// External libraries
import * as PIXI from "pixi.js";

'''

TypescriptSpritesheet = '''\
export const {0}: {{
    {1}
}} = {{
    {2}
}};
'''

TypescriptSpritesheetSeparator = '\n'
TypescriptFrameDelimiter = '\n    '


class Spritesheet(object):
    def __init__(self, folder):
        self.prefix = '_'.join(os.path.split(folder.path))
        self.name = os.path.basename(folder.path)
        self.frames = [
            SpritesheetFrame(frame.split('.png')[0])
            for frame in folder.get_files('.png')
        ]
        self.frames.sort(key=lambda frame: frame.name)

        # Error verification
        if len(self.frames) == 0:
            raise Warning('No frames found on folder {}'.format(folder.path))

    ######################
    # Typescript support #
    ######################
    @property
    def typescript(self):
        return TypescriptSpritesheet.format(
            self.name,
            self.typescriptDeclaration,
            self.typescriptDefinition)

    @property
    def typescriptDeclaration(self):
        transformed = [frame.typescriptDeclaration for frame in self.frames]
        return TypescriptFrameDelimiter.join(transformed)

    @property
    def typescriptDefinition(self):
        transformed = [frame.typescriptDefinition for frame in self.frames]
        return TypescriptFrameDelimiter.join(transformed)


FrameDeclarationTypescript = '{}: PIXI.Texture | undefined,'
FrameDefinitionTypescript = '{}: undefined,'


class SpritesheetFrame(object):
    def __init__(self, name):
        self.name = name

    ######################
    # Typescript support #
    ######################
    @property
    def typescriptDeclaration(self):
        return FrameDeclarationTypescript.format(self.name)

    @property
    def typescriptDefinition(self):
        return FrameDefinitionTypescript.format(self.name)


##################################
# Command-line execution support #
##################################

Description = '''\
Searches a folder or folder tree for .png files then generates declarations
and definitions for the resources found.'''


def main():
    boilerplate.parser.usage = '%prog [options] FOLDER'
    boilerplate.parser.description = Description
    boilerplate.add_output_option()
    boilerplate.add_recursive_option()
    boilerplate.add_verbose_option()

    (options, arguments) = boilerplate.parse_options(1)
    outstream = options.output
    in_folder = arguments[0].rstrip('/')

    # Write header
    outstream.write(TypescriptHeader)

    # Recursive execution
    if options.recursive:
        # Find leaf folders and grab structure information for them, grab only
        # folders with png files on them
        folders = [Folder(*info) for info in os.walk(in_folder)]
        folders = [folder for folder in folders if folder.is_leaf]

        # Filter to only folders with png files
        folders = [folder for folder in folders if folder.has_files('.png')]
        folders.sort(key=lambda folder: os.path.basename(folder.path))

        for folder in folders:
            boilerplate.debug('  Parsing folder "{}"\n'.format(folder.path))
            spritesheet = Spritesheet(folder)
            outstream.write(TypescriptSpritesheetSeparator)
            outstream.write(spritesheet.typescript)

    # Single-file execution
    else:
        walker = os.walk(in_folder)
        folder = Folder(*next(walker))
        boilerplate.debug('  Parsing folder "{}"\n'.format(folder.path))
        spritesheet = Spritesheet(folder)
        outstream.write(TypescriptSpritesheetSeparator)
        outstream.write(spritesheet.typescript)

    boilerplate.debug('  Spritesheet definitions written to "{}".\n'
                      .format(outstream.name))
    outstream.close()

if __name__ == '__main__':
    main()
