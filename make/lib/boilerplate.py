#!/usr/bin/env python3

import collections
import optparse
import os
import sys

parser = optparse.OptionParser()


class Settings(object):
    verbose = False


def debug(*args, **kargs):
    if Settings.verbose is True:
        sys.stdout.write(*args, **kargs)

################################
# Standardized option handling #
################################


def parse_options(arguments_wanted):
    if (
        (not isinstance(arguments_wanted, int)) and
        (not isinstance(arguments_wanted, collections.Sequence))
    ) or (isinstance(arguments_wanted, str)):
        raise ValueError('Unexpected type for arguments_wanted: {}'
                         .format(type(arguments_wanted)))

    (options, arguments) = parser.parse_args()

    # Validate argument quantity
    n_args = len(arguments)
    if (
        isinstance(arguments_wanted, int) and
        (n_args != arguments_wanted)
    ):
        raise ValueError('Unexpected number of arguments: {} - expected {}'
                         .format(n_args, arguments_wanted))
    elif (
        isinstance(arguments_wanted, collections.Sequence) and
        n_args not in arguments_wanted
    ):
        raise ValueError('Unexpected number of arguments: {} - expected {}'
                         .format(n_args, ', '.join(arguments_wanted)))

    # Parse verbose
    Settings.verbose = hasattr(options, 'verbose') and options.verbose is True

    # Overwrite 'output' argument with stdout or a valid output file stream
    if hasattr(options, 'output'):
        if options.output is None:
            options.output = sys.stdout
        else:
            dirname = os.path.dirname(options.output)
            if not os.path.exists(dirname):
                debug('  Create folder "{}"\n'.format(dirname))
                os.makedirs(dirname)
            options.output = open(options.output, 'wt')

        if Settings.verbose:
            debug('{}: Writing output to "{}"\n'.format(
                os.path.basename(sys.argv[0]),
                options.output.name))

    return (options, arguments)

########################
# Standardized options #
########################


def add_output_option():
    parser.add_option(
        '-o',
        '--output',
        dest='output',
        metavar='FILE',
        help='Output generated code to FILE',
        default=None,
    )


def add_recursive_option():
    parser.add_option(
        '-r',
        '--recursive',
        dest='recursive',
        help='Recursively iterate over child folders of given path and combine'
             'output into a single definition',
        action='store_true',
        default=False,
    )


def add_resource_path_option():
    parser.add_option(
        '-p',
        '--resource-path',
        dest='resource_path',
        metavar='PATH',
        help='Resource PATH that will be referenced by all generated code.'
             'This refers to the folder in the packaged build of your program',
        default='resources/',
    )


def add_verbose_option():
    parser.add_option(
        '-v',
        '--verbose',
        dest='verbose',
        help='Output progress messages to stdout',
        action='store_true',
        default=False,
    )

###########################################################
# Command-line execution support (for debugging purposes) #
###########################################################


def main():
    parser.usage = '%prog [options] arguments'
    parser.description = 'Test runner for our script boilerplate'
    add_verbose_option()
    add_output_option()
    add_load_path_option()
    (options, arguments) = parse_options(0)
    print("Options is:", options)
    print("Arguments is:", arguments)

if __name__ == '__main__':
    main()
