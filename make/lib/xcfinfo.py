#!/usr/bin/env python3

import os
import subprocess
import sys


class XcfInfo(object):
    def __init__(self, path):
        if not os.path.isfile(path):
            raise RuntimeError('Input path "{}" is not a file'.format(path))

        if not path.endswith('.xcf'):
            raise ValueError('Input file "{}" is not a xcf file'.format(path))

        # Extract simple file information
        self.name = os.path.basename(path).split('.xcf')[0]
        self.path = os.path.dirname(path)

        xcfinfo_output = subprocess.check_output(
            'xcfinfo {}'.format(path), shell=True
        ).decode()
        xcfinfo_lines = xcfinfo_output.split('\n')

        if not xcfinfo_lines[0].startswith('Version '):
            raise ValueError('Unexpected header on xcfinfo output:\n"{}"'
                             .format(xcfinfo_lines[0]))

        # Grab header information
        header_tokens = xcfinfo_lines[0].split()
        (image_width, image_height) = header_tokens[2].split('x')
        self.height = int(image_height)
        self.width = int(image_width)

        # Grab layer information
        self.layers = [
            XcfLayerInfo.from_xcfinfo(line)
            for line in xcfinfo_lines[1:]
            if len(line.strip()) > 0
        ]

    def __str__(self):
        return 'XcfInfo({}):\n width: {}\n height: {}\n layers:\n   {}'.format(
            self.path,
            self.width,
            self.height,
            '\n   '.join([str(layer) for layer in self.layers]))


class XcfLayerInfo(object):
    def __init__(self, xcf_layer_name, width, height, x, y):
        self.xcf_layer_name = xcf_layer_name
        self.width = width
        self.height = height
        self.x = x
        self.y = y

        tokens = xcf_layer_name.split()
        if len(tokens) == 1:
            self.name = xcf_layer_name
            self.layer_type = None
        elif len(tokens) == 2:
            self.name = tokens[0]
            self.layer_type = tokens[1]
        else:
            self.name = ''.join(tokens)
            self.layer_type = None
            # raise Warning('Renaming layer "{}" to "{}" for code generation'
            #     .format(self.xcf_layer_name, self.name))

    @classmethod
    def from_xcfinfo(cls, xcfinfo_line):
        tokens = xcfinfo_line.split()
        box = tokens[1]
        (dimensions, shift) = box.split('+', 1)
        (w, h) = dimensions.split('x')
        (x, y) = shift.split('+')
        name = ' '.join(tokens[4:])
        return cls(name, int(w), int(h), int(x), int(y))

    def __str__(self):
        if self.layer_type:
            return ('{} ({}): size: ({}, {}) - offset: ({}, {})'
                    .format(self.name, self.layer_type, self.width,
                            self.height, self.x, self.y))
        return('{}: size: ({}, {}) - offset: ({}, {})'
               .format(self.name, self.width, self.height, self.x, self.y))

if __name__ == '__main__':
    info = XcfInfo(sys.argv[1])
    print(info)
