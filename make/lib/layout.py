#!/usr/bin/env python3

import getopt
import os
import sys

from xcfinfo import XcfInfo
import boilerplate

######################
# Typescript support #
######################
TypescriptHeader = '''\
// Auto-generated file, do not edit directly. Run {} to update the information
// instead.

// Game libraries
import { ILayoutInfo } from "../layoutInfo";
'''

TypescriptLayoutGroupDeclaration = '''
export const {0}: {{
    width: number;
    height: number;
    {1}
}}'''

TypescriptLayoutGroupDefinition = '''\
{{
    width: {0},
    height: {1},
    {2}
}}'''


class LayoutGroup(object):
    def __init__(self, name, width, height, layouts):
        self.name = name
        self.width = width
        self.height = height
        self.layouts = layouts.copy()
        # if len(self.layouts) == 0:
        #     raise Warning('No layouts found in {}'.format(name))
        self.layouts.sort(key=lambda layout: layout.name)

    @classmethod
    def fromXcfInfo(cls, xcf_info):
        return cls(xcf_info.name, xcf_info.width, xcf_info.height, [
            Layout.fromXcfLayer(layer) for layer in xcf_info.layers
            if layer.layer_type == 'layout'
        ])

    def __str__(self):
        return 'LayoutGroup({}):\n   {}'.format(self.name, '\n   '.join(
            [str(layout) for layout in self.layouts]))

    ######################
    # Typescript support #
    ######################
    @property
    def typescript(self):
        return '{} = {};\n'.format(
            self.typescriptDeclaration,
            self.typescriptDefinition)

    @property
    def typescriptDeclaration(self):
        return TypescriptLayoutGroupDeclaration.format(
            self.name, self.typescriptLayoutDeclarations)

    @property
    def typescriptLayoutDeclarations(self):
        return '\n    '.join([
            layout.typescriptDeclaration for layout in self.layouts])

    @property
    def typescriptDefinition(self):
        return TypescriptLayoutGroupDefinition.format(
            self.width, self.height, self.typescriptLayoutDefinitions)

    @property
    def typescriptLayoutDefinitions(self):
        return '\n    '.join([
            layout.typescriptDefinition for layout in self.layouts])


TypescriptLayoutDeclaration = '{}: ILayoutInfo;'

TypescriptLayoutDefinition = '''\
{0}: {{
        width:   {1},
        height:  {2},
        left:    {3},
        top:     {4},
        centerX: {5},
        centerY: {6},
        right:   {7},
        bottom:  {8},
    }},'''


class Layout(object):
    def __init__(self, name, width, height, left, top):
        self.name = name
        self.width = width
        self.height = height
        self.left = left
        self.top = top
        self.center_x = int(left + (0.5 * width))
        self.center_y = int(top + (0.5 * height))
        self.right = left + width
        self.bottom = top + height

    @classmethod
    def fromXcfLayer(cls, xcf_layer):
        return cls(
            xcf_layer.name,
            xcf_layer.width,
            xcf_layer.height,
            xcf_layer.x,
            xcf_layer.y,
        )

    @property
    def typescriptDeclaration(self):
        return TypescriptLayoutDeclaration.format(self.name)

    @property
    def typescriptDefinition(self):
        return TypescriptLayoutDefinition.format(
            self.name,

            self.width,
            self.height,

            self.left,
            self.top,

            self.center_x,
            self.center_y,

            self.right,
            self.bottom)

##########################################
# Support execution as standalone script #
##########################################

Description = '''\
Extracts the following information from layers named "foo layout" in .xcf files
(gimp projects): Dimensions (width, height), top-left corners, centers, and
bottom-right corners.'''


def main():
    boilerplate.parser.usage = '%prog [options] XCF_FILE'
    boilerplate.parser.description = Description
    boilerplate.add_output_option()
    boilerplate.add_verbose_option()

    (options, arguments) = boilerplate.parse_options(1)
    outstream = options.output

    # Construct layout info
    xcf_info = XcfInfo(arguments[0])
    layout_group = LayoutGroup.fromXcfInfo(xcf_info)

    # Bail out early if we have no layouts to display
    if len(layout_group.layouts) == 0:
        boilerplate.debug('  No layout found in {}\n'.format(arguments[0]))
        # TODO take a -f, --fail-ok option to toggle returning 0/1 on fail
        sys.exit(0)

    outstream.write(layout_group.typescript)
    boilerplate.debug('  Layout definition written to "{}".\n'
                      .format(outstream.name))

if __name__ == '__main__':
    main()
