class Folder(object):
    def __init__(self, path, subfolders=[], filenames=[]):
        self.path = path
        self.subfolders = subfolders.copy()
        self.filenames = filenames.copy()

    def get_files(self, extension=None):
        if extension is not None:
            return [
                filename for filename in self.filenames
                if filename.endswith(extension)
            ]
        return self.filenames

    def has_files(self, extension=None):
        if len(self.filenames) == 0:
            return False

        if extension is None:
            return len(self.filenames) > 0

        for filename in self.filenames:
            if filename.endswith(extension):
                return True

        return False

    @property
    def is_leaf(self):
        return len(self.subfolders) == 0

    # Operator overloads
    def __repr__(self):
        return 'FolderInfo(path={0}, subfolders={1}, filenames={2})'.format(
            self.path, self.subfolders, self.filenames)
